/*
 ============================================================================
 Name		: TestStringAppView.cpp
 Author	  : 
 Copyright   : 
 Description : Application view implementation
 ============================================================================
 */

// INCLUDE FILES
#include <coemain.h>
#include "TestStringAppView.h"

// ============================ MEMBER FUNCTIONS ===============================

// -----------------------------------------------------------------------------
// CTestStringAppView::NewL()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CTestStringAppView* CTestStringAppView::NewL(const TRect& aRect)
	{
	CTestStringAppView* self = CTestStringAppView::NewLC(aRect);
	CleanupStack::Pop(self);
	return self;
	}

// -----------------------------------------------------------------------------
// CTestStringAppView::NewLC()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CTestStringAppView* CTestStringAppView::NewLC(const TRect& aRect)
	{
	CTestStringAppView* self = new (ELeave) CTestStringAppView;
	CleanupStack::PushL(self);
	self->ConstructL(aRect);
	return self;
	}

// -----------------------------------------------------------------------------
// CTestStringAppView::ConstructL()
// Symbian 2nd phase constructor can leave.
// -----------------------------------------------------------------------------
//
void CTestStringAppView::ConstructL(const TRect& aRect)
	{
	// Create a window for this application view
	CreateWindowL();

	// Set the windows size
	SetRect(aRect);

	// Activate the window, which makes it ready to be drawn
	ActivateL();
	}

// -----------------------------------------------------------------------------
// CTestStringAppView::CTestStringAppView()
// C++ default constructor can NOT contain any code, that might leave.
// -----------------------------------------------------------------------------
//
CTestStringAppView::CTestStringAppView()
	{
	// No implementation required
	}

// -----------------------------------------------------------------------------
// CTestStringAppView::~CTestStringAppView()
// Destructor.
// -----------------------------------------------------------------------------
//
CTestStringAppView::~CTestStringAppView()
	{
	// No implementation required
	}

// -----------------------------------------------------------------------------
// CTestStringAppView::Draw()
// Draws the display.
// -----------------------------------------------------------------------------
//
void CTestStringAppView::Draw(const TRect& /*aRect*/) const
	{
	// Get the standard graphics context
	CWindowGc& gc = SystemGc();

	// Gets the control's extent
	TRect drawRect(Rect());

	// Clears the screen
	gc.Clear(drawRect);

	}

// -----------------------------------------------------------------------------
// CTestStringAppView::SizeChanged()
// Called by framework when the view size is changed.
// -----------------------------------------------------------------------------
//
void CTestStringAppView::SizeChanged()
	{
	DrawNow();
	}

// -----------------------------------------------------------------------------
// CTestStringAppView::HandlePointerEventL()
// Called by framework to handle pointer touch events.
// Note: although this method is compatible with earlier SDKs, 
// it will not be called in SDKs without Touch support.
// -----------------------------------------------------------------------------
//
void CTestStringAppView::HandlePointerEventL(const TPointerEvent& aPointerEvent)
	{

	// Call base class HandlePointerEventL()
	CCoeControl::HandlePointerEventL(aPointerEvent);
	}

// End of File
