/*
 ============================================================================
 Name		: TestString.cpp
 Author	  : 
 Copyright   : 
 Description : Main application class
 ============================================================================
 */

// INCLUDE FILES
#include <eikstart.h>
#include "TestStringApplication.h"

LOCAL_C CApaApplication* NewApplication()
	{
	return new CTestStringApplication;
	}

GLDEF_C TInt E32Main()
	{
	return EikStart::RunApplication(NewApplication);
	}

