/*
 * IString8.cpp
 *
 *  Created on: 31.10.2012
 *      Author: ���
 */

#include "IString8.h"

IString8::IString8(const TDesC8& aDes)
{
    iBuf.Create(aDes);
}

IString8::IString8(const IString8& aDes)
{
    iBuf.Close();
    iBuf.Create(aDes.data());
}
IString8::IString8(int aNum)
{
    iBuf.Create(10);
    iBuf.AppendNum(aNum);
}

IString8::~IString8()
{
    iBuf.Close();
}

const TDesC8& IString8::data()const
{
    return iBuf;
}

IString8& IString8::operator=(const TDesC8& aDes)
{
    assign(aDes);
    return *this;
}

IString8& IString8::operator=(const IString8& aDes)
{
    assign(aDes.data());
    return *this;
}

IString8& IString8::operator+=(const TDesC8& aDes)
{
    conc(aDes);
    return *this;
}

IString8& IString8::operator+=(const IString8& aDes)
{
    conc(aDes.data());
    return *this;
}

IString8& IString8::operator+=(TInt32 aDes)
{
    RBuf8 aNum;
    aNum.Create(10);
    aNum.AppendNum(aDes);
    conc(aNum);
    aNum.Close();
    return *this;
}

const IString8& IString8::operator+(const IString8 &s1)
{ 
    *this += s1; 
    return *this; 
}

const IString8& IString8::operator+(const TDesC8 &s1)
{
    *this += s1; 
    return *this; 
}
const IString8& IString8::operator+(int aNum)
{
    *this += aNum; 
    return *this;
}

bool  IString8::operator== ( const TDesC8 & other ) const
{
    if(iBuf.Compare(other)==0)return true;
    return false;
}

bool  IString8::operator== ( const IString8 & other ) const
{
    if(iBuf.Compare(other.data())==0)return true;
    return false;
}

TInt IString8::count()
{
    return iBuf.Length();
}
TInt32 IString8::toInt(TRadix aRadix)
{
    TLex8 conv;
    TInt32 IntVal;
    conv.Assign(iBuf);
    conv.Val(IntVal,aRadix);
    return IntVal;
}
void IString8::conc(const TDesC8& aDes)
{
    RBuf8 tempBuf;
    tempBuf.Create(aDes.Length()+iBuf.Length());
    tempBuf.Append(iBuf);
    tempBuf.Append(aDes);
    iBuf.Close();
    iBuf.Create(tempBuf);
    tempBuf.Close();
}
void IString8::assign(const TDesC8& aDes)
{
    RBuf8 tempBuf;tempBuf.Create(aDes);
    iBuf.Close();
    iBuf.Create(tempBuf);
    tempBuf.Close();
}
CArrayPtrFlat<IString8>*IString8::split(const TDesC8& aDes)
{
    RBuf8 splitBuf; 
    splitBuf.Create(iBuf);
    CArrayPtrFlat<IString8>*splitArray= new (ELeave)CArrayPtrFlat<IString8>(10);
    
    while(splitBuf.Size()>0){
        int iPos=splitBuf.Find(aDes);
        if(iPos==-1){
            if(splitBuf.Size()>0)
                splitArray->AppendL(new IString8(splitBuf));
            break;
        }
        if(iPos>0){
            RBuf8 part; part.Create(iPos+1);
            part.Copy(splitBuf.Left(iPos));
            splitArray->AppendL(new IString8(part));
            part.Close();
        }
        splitBuf.Delete(0,iPos+aDes.Length());
        if(splitBuf.Size()==0)break;
    }
    splitBuf.Close();
    splitArray->Compress();
    return splitArray;
}
void IString8::merge(CArrayPtrFlat<IString8>*aArray,const TDesC8& aSplitter)
{
    IString8 mergeStr;
    for(int i=0;i<aArray->Length();i++){
        mergeStr+=*((*aArray)[i]);
        if(i!=aArray->Length()-1)
            mergeStr+=aSplitter;
    }
    iBuf.Close();
    iBuf.Create(mergeStr.data());
}
