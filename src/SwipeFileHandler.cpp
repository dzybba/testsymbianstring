/*
 ============================================================================
 Name		: SwipeFileHandler.cpp
 Author	  : ����� ���������
 Version	 : 1.0
 Copyright   : 
 Description : CSwipeFileHandler implementation
 ============================================================================
 */

#include "SwipeFileHandler.h"
#include "StringToFile.h"
_LIT8(KLandScapeMode,"isLandscapeMode");
_LIT8(KStartRect,"StartRect");
_LIT8(KEndRect,"EndRect");
_LIT8(KStartRectLand,"StartRectLand");
_LIT8(KEndRectLand,"EndRectLand");
_LIT8(KTypeAction,"TypeAction");
_LIT8(KUid,"Uid");
_LIT8(KSpaser," ");
_LIT8(KEnter,"\n");

CSwipeFileHandler::CSwipeFileHandler()
{
    iFile=NULL;
}

CSwipeFileHandler::~CSwipeFileHandler()
{
    if(iFile){
        delete iFile;
        iFile=NULL;
    }
}

CSwipeFileHandler* CSwipeFileHandler::NewLC(const TDesC& aLocation)
{
    CSwipeFileHandler* self = new (ELeave) CSwipeFileHandler();
    CleanupStack::PushL(self);
    self->ConstructL(aLocation);
    return self;
}

CSwipeFileHandler* CSwipeFileHandler::NewL(const TDesC& aLocation)
{
    CSwipeFileHandler* self = CSwipeFileHandler::NewLC(aLocation);
    CleanupStack::Pop(); // self;
    return self;
}

void CSwipeFileHandler::ConstructL(const TDesC& aLocation)
{
    iFile=CStringToFile::NewL(aLocation);
    loadIniData();
}

bool CSwipeFileHandler::isLandscapeModeOn()
{
    return iIniData.iIsLandscape;
}

TRect& CSwipeFileHandler::getStartRect()
{
    return iIniData.iStartRect;
}

TRect& CSwipeFileHandler::getEndRect()
{
    return iIniData.iEndRect;
}

TRect& CSwipeFileHandler::getStartRectLandscape()
{
    return iIniData.iStartRectLandscape;
}

TRect& CSwipeFileHandler::getEndRectLandscape()
{
    return iIniData.iEndRectLandscape;
}

TInt CSwipeFileHandler::getTypeAction()
{
    return iIniData.iType;
}

TInt32 CSwipeFileHandler::getUid()
{
    return iIniData.iUid;
}

void CSwipeFileHandler::setLandscapeMode(TBool aState)
{
    iIniData.iIsLandscape=aState;
}

void CSwipeFileHandler::setStartRect(TRect aRect)
{
    iIniData.iStartRect=aRect;
}
void CSwipeFileHandler::setEndRect(TRect aRect)
{
    iIniData.iEndRect=aRect;
}

void CSwipeFileHandler::setStartRectLandscape(TRect aRect)
{
    iIniData.iStartRectLandscape=aRect;
}
void CSwipeFileHandler::setEndRectLandscape(TRect aRect)
{
    iIniData.iEndRectLandscape=aRect;
}

void CSwipeFileHandler::setTypeAction(int aAction)
{
    iIniData.iType=aAction;
}
void CSwipeFileHandler::setUid(TInt32 aUid)
{
    iIniData.iUid=aUid;
}

void CSwipeFileHandler::loadIniData()
{
    IString8 iBufferString;
    iFile->readAllFile(iBufferString);
    CArrayPtrFlat<IString8>* iTempBuffer=iBufferString.split(KEnter);
    for(TInt i=0;i<iTempBuffer->Count();i++){
        CArrayPtrFlat<IString8>* iStrings=iTempBuffer->At(i)->split(KSpaser);
        parseData(iStrings);
        iStrings->ResetAndDestroy();
        delete iStrings;
    }
    iTempBuffer->ResetAndDestroy();
    delete iTempBuffer;
}

void CSwipeFileHandler::parseData(CArrayPtrFlat<IString8>*aData)
{
    if(aData->Count()>0){
        if(*(aData->At(0))==KLandScapeMode){
            TInt tempVal;
            parseInt(aData,tempVal);
            iIniData.iIsLandscape=tempVal;
        }
        else if(*(aData->At(0))==KStartRect)
            parseRects(aData,iIniData.iStartRect);
        else if(*(aData->At(0))==KEndRect)
            parseRects(aData,iIniData.iEndRect);
        else if(*(aData->At(0))==KStartRectLand)
            parseRects(aData,iIniData.iStartRectLandscape);
        else if(*(aData->At(0))==KEndRectLand)
            parseRects(aData,iIniData.iEndRectLandscape);
        else if(*(aData->At(0))==KTypeAction)
            parseInt(aData,iIniData.iType);
        else if(*(aData->At(0))==KUid)
            parseInt32(aData,iIniData.iUid);
    }
}
void CSwipeFileHandler::parseRects(CArrayPtrFlat<IString8>*aData,TRect&aRect)
{
    if(aData->Count()>=5){
        aRect.SetRect(TPoint(aData->At(1)->toInt(),
            aData->At(2)->toInt()),
            TSize(aData->At(3)->toInt(),
                aData->At(4)->toInt()));
    }
}
void CSwipeFileHandler::parseInt(CArrayPtrFlat<IString8>*aData,TInt &aVal)
{
    if(aData->Count()>=2){
        aVal=aData->At(1)->toInt();
    }
}
void CSwipeFileHandler:: parseInt32(CArrayPtrFlat<IString8>*aData,TInt32 &aVal)
{
    if(aData->Count()>=2){
        aVal=aData->At(1)->toInt();
    }
}

void CSwipeFileHandler::saveIniData()
{
    IString8 tempIniData;
    fromIntToString(KLandScapeMode,tempIniData,iIniData.iIsLandscape);
    fromRectToString(KStartRect,tempIniData,iIniData.iStartRect);
    fromRectToString(KEndRect,tempIniData,iIniData.iEndRect);
    fromRectToString(KStartRectLand,tempIniData,iIniData.iStartRectLandscape);
    fromRectToString(KEndRectLand,tempIniData,iIniData.iEndRectLandscape);
    fromIntToString(KTypeAction,tempIniData,iIniData.iType);
    fromIntToString(KUid,tempIniData,iIniData.iUid);
    iFile->writeAllFile(tempIniData);
}

void CSwipeFileHandler::fromRectToString(const TDesC8&aName,IString8&aStr,TRect&aRect)
{
    aStr+=aName;aStr+=KSpaser;
    aStr+=aRect.iTl.iX;aStr+=KSpaser;
    aStr+=aRect.iTl.iY;aStr+=KSpaser;
    aStr+=aRect.Width();aStr+=KSpaser;
    aStr+=aRect.Height();aStr+=KEnter;
}
void CSwipeFileHandler::fromIntToString(const TDesC8&aName,IString8&aStr,TInt32 aVal)
{
    aStr+=aName;
    aStr+=KSpaser;
    aStr+=aVal;
    aStr+=KEnter;
}
