/*
 ============================================================================
 Name		: BufString.cpp
 Author	  : 
 Version	 : 1.0
 Copyright   : 
 Description : CBufString implementation
 ============================================================================
 */

#include "BufString.h"

IString::IString(const TDesC& aDes)
{
    iBuf.Create(aDes);
}

IString::IString(const IString& aDes)
{
    iBuf.Close();
    iBuf.Create(aDes.data());
}

IString::~IString()
{
    iBuf.Close();
}

const TDesC& IString::data()const
{
    return iBuf;
}

IString& IString::operator=(const TDesC& aDes)
{
    assign(aDes);
    return *this;
}

IString& IString::operator=(const IString& aDes)
{
    assign(aDes.data());
    return *this;
}

IString& IString::operator+=(const TDesC& aDes)
{
    conc(aDes);
    return *this;
}

IString& IString::operator+=(const IString& aDes)
{
    conc(aDes.data());
    return *this;
}

const IString& IString::operator+(const IString &s1)
{ 
    *this += s1; 
    return *this; 
}

const IString& IString::operator+(const TDesC &s1)
{
    *this += s1; 
    return *this; 
}
bool  IString::operator== ( const TDesC & other ) const
{
    if(iBuf.Compare(other)==0)return true;
    return false;
}

bool  IString::operator== ( const IString & other ) const
{
    if(iBuf.Compare(other.data())==0)return true;
    return false;
}

TInt IString::count()
{
    return iBuf.Length();
}

void IString::conc(const TDesC& aDes)
{
    RBuf tempBuf;
    tempBuf.Create(aDes.Length()+iBuf.Length());
    tempBuf.Append(iBuf);
    tempBuf.Append(aDes);
    iBuf.Close();
    iBuf.Create(tempBuf);
    tempBuf.Close();
}
void IString::assign(const TDesC& aDes)
{
    RBuf tempBuf;tempBuf.Create(aDes);
    iBuf.Close();
    iBuf.Create(tempBuf);
    tempBuf.Close();
}
