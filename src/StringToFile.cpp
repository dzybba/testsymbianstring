/*
 ============================================================================
 Name		: StringToFile.cpp
 Author	  : ����� ���������
 Version	 : 1.0
 Copyright   : 
 Description : CStringToFile implementation
 ============================================================================
 */

#include "StringToFile.h"

CStringToFile::CStringToFile()
{
    // No implementation required
}

CStringToFile::~CStringToFile()
{
}

CStringToFile* CStringToFile::NewLC(const TDesC& aLocation)
{
    CStringToFile* self = new (ELeave) CStringToFile();
    CleanupStack::PushL(self);
    self->ConstructL(aLocation);
    return self;
}

CStringToFile* CStringToFile::NewL(const TDesC& aLocation)
{
    CStringToFile* self = CStringToFile::NewLC(aLocation);
    CleanupStack::Pop(); // self;
    return self;
}

void CStringToFile::ConstructL(const TDesC& aLocation)
{
    iLocation.Create(aLocation);
    User::LeaveIfError(fs.Connect());
    TInt fres=file.Create(fs,iLocation,EFileRead|EFileShareReadersOrWriters);
    if(fres==KErrNone)
    {
        file.Write(_L8(""));
        file.Close();
    }
}

void CStringToFile::readAllFile(IString8& aString)
{
    myOpenFileL(EFileRead);
    TInt len;
    file.Size(len);//size of file
    RBuf8 tempBuf;
    tempBuf.Create(len+1);
    file.Read(tempBuf);
    aString+=tempBuf;
    tempBuf.Close();
    myCloseFileL();
}

void CStringToFile::writeAllFile(IString8&aString)
{
    myOpenFileL(EFileWrite);
    file.SetSize(0);//nulled file
    file.Write(aString.data());
    myCloseFileL();
}

void CStringToFile::myCloseFileL()
{
    file.Close();
}

void CStringToFile::myOpenFileL(TInt aFlag)
{
    file.Open(fs,iLocation,aFlag|EFileShareAny);
}
