/*
 ============================================================================
 Name		: TestStringApplication.cpp
 Author	  : 
 Copyright   : 
 Description : Main application class
 ============================================================================
 */

// INCLUDE FILES
#include "TestString.hrh"
#include "TestStringDocument.h"
#include "TestStringApplication.h"

// ============================ MEMBER FUNCTIONS ===============================

// -----------------------------------------------------------------------------
// CTestStringApplication::CreateDocumentL()
// Creates CApaDocument object
// -----------------------------------------------------------------------------
//
CApaDocument* CTestStringApplication::CreateDocumentL()
	{
	// Create an TestString document, and return a pointer to it
	return CTestStringDocument::NewL(*this);
	}

// -----------------------------------------------------------------------------
// CTestStringApplication::AppDllUid()
// Returns application UID
// -----------------------------------------------------------------------------
//
TUid CTestStringApplication::AppDllUid() const
	{
	// Return the UID for the TestString application
	return KUidTestStringApp;
	}

// End of File
