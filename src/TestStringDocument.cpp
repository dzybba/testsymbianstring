/*
 ============================================================================
 Name		: TestStringDocument.cpp
 Author	  : 
 Copyright   : 
 Description : CTestStringDocument implementation
 ============================================================================
 */

// INCLUDE FILES
#include "TestStringAppUi.h"
#include "TestStringDocument.h"

// ============================ MEMBER FUNCTIONS ===============================

// -----------------------------------------------------------------------------
// CTestStringDocument::NewL()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CTestStringDocument* CTestStringDocument::NewL(CEikApplication& aApp)
	{
	CTestStringDocument* self = NewLC(aApp);
	CleanupStack::Pop(self);
	return self;
	}

// -----------------------------------------------------------------------------
// CTestStringDocument::NewLC()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CTestStringDocument* CTestStringDocument::NewLC(CEikApplication& aApp)
	{
	CTestStringDocument* self = new (ELeave) CTestStringDocument(aApp);

	CleanupStack::PushL(self);
	self->ConstructL();
	return self;
	}

// -----------------------------------------------------------------------------
// CTestStringDocument::ConstructL()
// Symbian 2nd phase constructor can leave.
// -----------------------------------------------------------------------------
//
void CTestStringDocument::ConstructL()
	{
	// No implementation required
	}

// -----------------------------------------------------------------------------
// CTestStringDocument::CTestStringDocument()
// C++ default constructor can NOT contain any code, that might leave.
// -----------------------------------------------------------------------------
//
CTestStringDocument::CTestStringDocument(CEikApplication& aApp) :
	CAknDocument(aApp)
	{
	// No implementation required
	}

// ---------------------------------------------------------------------------
// CTestStringDocument::~CTestStringDocument()
// Destructor.
// ---------------------------------------------------------------------------
//
CTestStringDocument::~CTestStringDocument()
	{
	// No implementation required
	}

// ---------------------------------------------------------------------------
// CTestStringDocument::CreateAppUiL()
// Constructs CreateAppUi.
// ---------------------------------------------------------------------------
//
CEikAppUi* CTestStringDocument::CreateAppUiL()
	{
	// Create the application user interface, and return a pointer to it;
	// the framework takes ownership of this object
	return new (ELeave) CTestStringAppUi;
	}

// End of File
