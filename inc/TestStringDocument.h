/*
 ============================================================================
 Name		: TestStringDocument.h
 Author	  : 
 Copyright   : 
 Description : Declares document class for application.
 ============================================================================
 */

#ifndef __TESTSTRINGDOCUMENT_h__
#define __TESTSTRINGDOCUMENT_h__

// INCLUDES
#include <akndoc.h>

// FORWARD DECLARATIONS
class CTestStringAppUi;
class CEikApplication;

// CLASS DECLARATION

/**
 * CTestStringDocument application class.
 * An instance of class CTestStringDocument is the Document part of the
 * AVKON application framework for the TestString example application.
 */
class CTestStringDocument : public CAknDocument
	{
public:
	// Constructors and destructor

	/**
	 * NewL.
	 * Two-phased constructor.
	 * Construct a CTestStringDocument for the AVKON application aApp
	 * using two phase construction, and return a pointer
	 * to the created object.
	 * @param aApp Application creating this document.
	 * @return A pointer to the created instance of CTestStringDocument.
	 */
	static CTestStringDocument* NewL(CEikApplication& aApp);

	/**
	 * NewLC.
	 * Two-phased constructor.
	 * Construct a CTestStringDocument for the AVKON application aApp
	 * using two phase construction, and return a pointer
	 * to the created object.
	 * @param aApp Application creating this document.
	 * @return A pointer to the created instance of CTestStringDocument.
	 */
	static CTestStringDocument* NewLC(CEikApplication& aApp);

	/**
	 * ~CTestStringDocument
	 * Virtual Destructor.
	 */
	virtual ~CTestStringDocument();

public:
	// Functions from base classes

	/**
	 * CreateAppUiL
	 * From CEikDocument, CreateAppUiL.
	 * Create a CTestStringAppUi object and return a pointer to it.
	 * The object returned is owned by the Uikon framework.
	 * @return Pointer to created instance of AppUi.
	 */
	CEikAppUi* CreateAppUiL();

private:
	// Constructors

	/**
	 * ConstructL
	 * 2nd phase constructor.
	 */
	void ConstructL();

	/**
	 * CTestStringDocument.
	 * C++ default constructor.
	 * @param aApp Application creating this document.
	 */
	CTestStringDocument(CEikApplication& aApp);

	};

#endif // __TESTSTRINGDOCUMENT_h__
// End of File
