/*
 ============================================================================
 Name		: SwipeFileHandler.h
 Author	  : ����� ���������
 Version	 : 1.0
 Copyright   : 
 Description : CSwipeFileHandler declaration
 ============================================================================
 */

#ifndef SWIPEFILEHANDLER_H
#define SWIPEFILEHANDLER_H

// INCLUDES
#include <e32std.h>
#include <e32base.h>
#include "IString8.h"
class CStringToFile;

class CSwipeFileHandler: public CBase
{
public:
    ~CSwipeFileHandler();
    static CSwipeFileHandler* NewL(const TDesC& aLocation);

    static CSwipeFileHandler* NewLC(const TDesC& aLocation);
public:
    bool   isLandscapeModeOn();
    TRect& getStartRect();
    TRect& getEndRect();
    TRect& getStartRectLandscape();
    TRect& getEndRectLandscape();
    TInt   getTypeAction();
    TInt32 getUid();
public:
    void setLandscapeMode(TBool aState);
    void setStartRect(TRect aRect);
    void setEndRect(TRect aRect);
    void setStartRectLandscape(TRect aRect);
    void setEndRectLandscape(TRect aRect);
    void setTypeAction(int aAction);
    void setUid(TInt32 aUid);
public:
    void loadIniData();
    void saveIniData();
private:
    void parseData(CArrayPtrFlat<IString8>*aData);
    void parseRects(CArrayPtrFlat<IString8>*aData,TRect&aRect);
    void parseInt(CArrayPtrFlat<IString8>*aData,TInt &aVal);
    void parseInt32(CArrayPtrFlat<IString8>*aData,TInt32 &aVal);
    void fromRectToString(const TDesC8&aName,IString8&aStr,TRect&aRect);
    void fromIntToString(const TDesC8&aName,IString8&aStr,TInt32 aVal);
public:
    struct iniData{       
        bool iIsLandscape;
        TRect iStartRect;
        TRect iEndRect;
        TRect iStartRectLandscape;
        TRect iEndRectLandscape;
        TInt  iType;
        TInt32 iUid;
        iniData(){
            iIsLandscape=false;
            iStartRect=TRect(0,0,0,0);
            iEndRect=TRect(0,0,0,0);
            iStartRectLandscape=TRect(0,0,0,0);
            iEndRectLandscape=TRect(0,0,0,0);
            iType=0;
            iUid=0;
        }
    };
private:
    CSwipeFileHandler();

    void ConstructL(const TDesC& aLocation);
private:
    CStringToFile*  iFile;
    iniData         iIniData;
};

#endif // SWIPEFILEHANDLER_H
