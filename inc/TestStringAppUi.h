/*
 ============================================================================
 Name		: TestStringAppUi.h
 Author	  : 
 Copyright   : 
 Description : Declares UI class for application.
 ============================================================================
 */

#ifndef __TESTSTRINGAPPUI_h__
#define __TESTSTRINGAPPUI_h__

// INCLUDES
#include <aknappui.h>

// FORWARD DECLARATIONS
class CTestStringAppView;

// CLASS DECLARATION
/**
 * CTestStringAppUi application UI class.
 * Interacts with the user through the UI and request message processing
 * from the handler class
 */
class CTestStringAppUi : public CAknAppUi
	{
public:
	// Constructors and destructor

	/**
	 * ConstructL.
	 * 2nd phase constructor.
	 */
	void ConstructL();

	/**
	 * CTestStringAppUi.
	 * C++ default constructor. This needs to be public due to
	 * the way the framework constructs the AppUi
	 */
	CTestStringAppUi();

	/**
	 * ~CTestStringAppUi.
	 * Virtual Destructor.
	 */
	virtual ~CTestStringAppUi();

private:
	// Functions from base classes

	/**
	 * From CEikAppUi, HandleCommandL.
	 * Takes care of command handling.
	 * @param aCommand Command to be handled.
	 */
	void HandleCommandL(TInt aCommand);

	/**
	 *  HandleStatusPaneSizeChange.
	 *  Called by the framework when the application status pane
	 *  size is changed.
	 */
	void HandleStatusPaneSizeChange();

	/**
	 *  From CCoeAppUi, HelpContextL.
	 *  Provides help context for the application.
	 *  size is changed.
	 */
	CArrayFix<TCoeHelpContext>* HelpContextL() const;

private:
	// Data

	/**
	 * The application view
	 * Owned by CTestStringAppUi
	 */
	CTestStringAppView* iAppView;

	};

#endif // __TESTSTRINGAPPUI_h__
// End of File
