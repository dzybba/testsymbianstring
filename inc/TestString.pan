/*
 ============================================================================
 Name		: TestString.pan
 Author	  : 
 Copyright   : 
 Description : This file contains panic codes.
 ============================================================================
 */

#ifndef __TESTSTRING_PAN__
#define __TESTSTRING_PAN__

/** TestString application panic codes */
enum TTestStringPanics
	{
	ETestStringUi = 1
	// add further panics here
	};

inline void Panic(TTestStringPanics aReason)
	{
	_LIT(applicationName, "TestString");
	User::Panic(applicationName, aReason);
	}

#endif // __TESTSTRING_PAN__
