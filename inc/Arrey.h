/*
 * Obj.h
 *
 *  Created on: 22.03.2012
 *      Author: Den
 */

#ifndef ARREY_H_
#define ARREY_H_

#include <e32std.h>
#include <e32base.h>

template <class T>
class PointArrey:public RPointerArray<T>
	{
public:
	~PointArrey(){this->Close();}
public://присваеваем значения
	PointArrey<T> & operator =(RPointerArray<T>& val){copyArr(val);return *this;}
	PointArrey<T> & operator =(PointArrey<T> & val){copyArr(val);return *this;	}
	void copyArr(RPointerArray<T>& val)
		{
		this->Reset();
		for(TInt i=0;i<val.Count();i++)
			{
			Append(val[i]);
			}
		}
	};

template <class T>
class Arrey:public RArray<T>
    {
public:
    ~Arrey(){
        this->Close();
    }
public://присваеваем значения
    Arrey<T> & operator =(RArray<T>& val){
        copyArr(val);
        return *this;
    }
    Arrey<T> & operator =(Arrey<T> & val){
        copyArr(val);
        return *this;}
  //  Arrey<T> & operator =(Arrey<T> val){copyArr(val);return *this;}
    void copyArr(RArray<T>& val)
        {
        this->Reset();
        for(TInt i=0;i<val.Count();i++)
            {
            Append(val[i]);
            }
        }
    };
#endif /* OBJ_H_ */
