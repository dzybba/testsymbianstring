/*
 ============================================================================
 Name		: TestStringApplication.h
 Author	  : 
 Copyright   : 
 Description : Declares main application class.
 ============================================================================
 */

#ifndef __TESTSTRINGAPPLICATION_H__
#define __TESTSTRINGAPPLICATION_H__

// INCLUDES
#include <aknapp.h>
#include "TestString.hrh"

// UID for the application;
// this should correspond to the uid defined in the mmp file
const TUid KUidTestStringApp =
	{
	_UID3
	};

// CLASS DECLARATION

/**
 * CTestStringApplication application class.
 * Provides factory to create concrete document object.
 * An instance of CTestStringApplication is the application part of the
 * AVKON application framework for the TestString example application.
 */
class CTestStringApplication : public CAknApplication
	{
public:
	// Functions from base classes

	/**
	 * From CApaApplication, AppDllUid.
	 * @return Application's UID (KUidTestStringApp).
	 */
	TUid AppDllUid() const;

protected:
	// Functions from base classes

	/**
	 * From CApaApplication, CreateDocumentL.
	 * Creates CTestStringDocument document object. The returned
	 * pointer in not owned by the CTestStringApplication object.
	 * @return A pointer to the created document object.
	 */
	CApaDocument* CreateDocumentL();
	};

#endif // __TESTSTRINGAPPLICATION_H__
// End of File
