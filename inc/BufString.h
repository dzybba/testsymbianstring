/*
 ============================================================================
 Name		: BufString.h
 Author	  : 
 Version	 : 1.0
 Copyright   : 
 Description : CBufString declaration
 ============================================================================
 */

#ifndef BUFSTRING_H
#define BUFSTRING_H

// INCLUDES
#include <e32std.h>
#include <e32base.h>

// CLASS DECLARATION

/**
 *  CBufString
 * 
 */
class IString
{
public:
    IString(const TDesC& aDes);
    IString(const IString& aDes);
    ~IString();	
private:
    RBuf	iBuf;
public:
    const TDesC& data()const;
    IString& operator=(const TDesC& aDes);
    IString& operator=(const IString& aDes);
    IString& operator+=(const TDesC& aDes);
    IString& operator+=(const IString& aDes);
    const IString& operator+(const IString &s1);
    const IString& operator+(const TDesC &s1);
    bool  operator== ( const TDesC & other ) const;
    bool  operator== ( const IString & other ) const;
public:
    TInt count();
private:
    void conc(const TDesC& aDes);
    void assign(const TDesC& aDes);
};

#endif // BUFSTRING_H
