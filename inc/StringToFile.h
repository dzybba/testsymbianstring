/*
 ============================================================================
 Name		: StringToFile.h
 Author	  : ����� ���������
 Version	 : 1.0
 Copyright   : 
 Description : CStringToFile declaration
 ============================================================================
 */

#ifndef STRINGTOFILE_H
#define STRINGTOFILE_H

// INCLUDES
#include <e32std.h>
#include <e32base.h>
#include <f32file.h>
#include <badesca.h>
#include "IString8.h"
// CLASS DECLARATION

/**
 *  CStringToFile
 * 
 */
class CStringToFile: public CBase
{
public:
    ~CStringToFile();
    static CStringToFile* NewL(const TDesC& aLocation);
    static CStringToFile* NewLC(const TDesC& aLocation);
public:
    void readAllFile(IString8& aString);
    void writeAllFile(IString8&aString);
private:
    CStringToFile();
    void ConstructL(const TDesC& aLocation);
private:
    void myCloseFileL();
    void myOpenFileL(TInt aFlag);
private:
    RBuf  iLocation;
    RFile file;
    RFs fs;
};

#endif // STRINGTOFILE_H
