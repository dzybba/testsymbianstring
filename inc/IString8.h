/*
 * IString8.h
 *
 *  Created on: 31.10.2012
 *      Author: ���
 */

#ifndef ISTRING8_H_
#define ISTRING8_H_
#include <e32std.h>
#include <e32base.h>
#include "Arrey.h"

class IString8
{
public:
    IString8(const TDesC8& aDes=_L8(""));
    IString8(const IString8& aDes);
    IString8(int aNum);
    ~IString8(); 
private:
    RBuf8    iBuf;
public:
    const TDesC8& data()const;
    IString8& operator=(const TDesC8& aDes);
    IString8& operator=(const IString8& aDes);
    IString8& operator+=(const TDesC8& aDes);
    IString8& operator+=(const IString8& aDes);
    IString8& operator+=(TInt32);
    const IString8& operator+(const IString8 &s1);
    const IString8& operator+(const TDesC8 &s1);
    const IString8& operator+(int);
    bool  operator== ( const TDesC8 & other ) const;
    bool  operator== ( const IString8 & other ) const;
public:
    CArrayPtrFlat<IString8>*split(const TDesC8& aDes);
    void merge(CArrayPtrFlat<IString8>*aArray,const TDesC8& aSplitter);
public:
    TInt count();
    TInt32 toInt(TRadix aRadix=EDecimal);
private:
    void conc(const TDesC8& aDes);
    void assign(const TDesC8& aDes);
};

#endif /* ISTRING8_H_ */
