# ============================================================================
#  Name	 : build_help.mk
#  Part of  : TestString
# ============================================================================
#  Name	 : build_help.mk
#  Part of  : TestString
#
#  Description: This make file will build the application help file (.hlp)
# 
# ============================================================================

do_nothing :
	@rem do_nothing

# build the help from the MAKMAKE step so the header file generated
# will be found by cpp.exe when calculating the dependency information
# in the mmp makefiles.

MAKMAKE : TestString_0xe1a54c01.hlp
TestString_0xe1a54c01.hlp : TestString.xml TestString.cshlp Custom.xml
	cshlpcmp TestString.cshlp
ifeq (WINSCW,$(findstring WINSCW, $(PLATFORM)))
	md $(EPOCROOT)epoc32\$(PLATFORM)\c\resource\help
	copy TestString_0xe1a54c01.hlp $(EPOCROOT)epoc32\$(PLATFORM)\c\resource\help
endif

BLD : do_nothing

CLEAN :
	del TestString_0xe1a54c01.hlp
	del TestString_0xe1a54c01.hlp.hrh

LIB : do_nothing

CLEANLIB : do_nothing

RESOURCE : do_nothing
		
FREEZE : do_nothing

SAVESPACE : do_nothing

RELEASABLES :
	@echo TestString_0xe1a54c01.hlp

FINAL : do_nothing
